from django import template
import urllib2
from django.utils.datastructures import SortedDict

register = template.Library()

@register.filter(name='unquote_new')
def unquote_new(value):
    return urllib2.unquote(value)


@register.filter(name='sort')
def listsort(value):
    if isinstance(value, dict):
        new_dict = SortedDict()
        key_list = sorted(value.keys())
        for key in key_list:
            new_dict[key] = value[key]
        return new_dict
    elif isinstance(value, list):
        return sorted(value)
    else:
        return value
    listsort.is_safe = True