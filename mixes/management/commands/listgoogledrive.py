from django.core.management.base import BaseCommand, CommandError
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive


class Command(BaseCommand):
	help = 'List google drive account files. Usefull to acquire file/folder ID.'

	def handle(self, *args, **options):
		gauth = GoogleAuth()
		gauth.LocalWebserverAuth()
		drive = GoogleDrive(gauth)

		file_list = drive.ListFile({'q': "'root' in parents"}).GetList()
		for file1 in file_list:
		  print 'title: %s, id: %s' % (file1['title'], file1['id'])