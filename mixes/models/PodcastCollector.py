from django.db import models
from django.utils.translation import ugettext_lazy as _


class PodcastCollector(models.Model):
    """
    Podcast names
    """
    id = models.AutoField(primary_key=True)
    source = models.CharField(
        max_length=2048,
        default='',
        help_text='(https://soundcloud.com/neinrecords)'
    )
    name = models.CharField(max_length=2048, default='', blank=True)
    desc = models.CharField(max_length=5192, default='', blank=True)
    thumb = models.CharField(max_length=2048, default='', blank=True)

    create_date = models.DateTimeField(auto_now_add=True, blank=True)

    def __unicode__(self):
        return self.name