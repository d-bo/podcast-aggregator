from django.db import models
from django.utils.translation import ugettext_lazy as _


class Label(models.Model):
    """
    Labels
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=2048, default='', blank=True)
    desc = models.CharField(max_length=5192, default='', blank=True)
    img = models.CharField(max_length=2048, default='', blank=True)

    def __unicode__(self):
        return self.name