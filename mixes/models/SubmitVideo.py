from django.db import models
from django.utils.translation import ugettext_lazy as _


class SubmitVideo(models.Model):
    id = models.AutoField(primary_key=True)
    link = models.CharField('', max_length=2048)

    def __str__(self):
        return self.link