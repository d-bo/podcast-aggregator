from django.db import models
from django.utils.translation import ugettext_lazy as _
from mixes.models.Genres import Genres


class Video(models.Model):
    """
    Video
    """
    id = models.AutoField(primary_key=True)
    title = models.CharField(
        max_length=2048,
        help_text = 'Title',
        blank=True
    )
    embed = models.CharField(
        max_length=2048,
        default='',
        help_text='(http://www.youtube.com/watch?v=5YYTEp4PFXw)'
    )
    embed_id = models.CharField(
        max_length=1024,
        default='',
        help_text='(youtube id: JIPZruqrZ5w)'
    )
    genre = models.ManyToManyField(Genres, blank=True)
    create_date = models.DateTimeField(auto_now_add=True, blank=True)

    def __unicode__(self):
        return self.title