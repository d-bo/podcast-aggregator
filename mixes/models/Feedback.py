from django.db import models
from django.utils.translation import ugettext_lazy as _


class Feedback(models.Model):
    id = models.AutoField(primary_key=True)
    email = models.EmailField(_('Your email'), max_length=512)
    desc = models.CharField(_('Text'), max_length=2048)

    def __str__(self):
        return self.email