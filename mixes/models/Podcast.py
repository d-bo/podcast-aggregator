from django.db import models
from django.utils.translation import ugettext_lazy as _

from mixes.models.Genres import Genres
from mixes.models.PodcastCollector import PodcastCollector
from mixes.models.Artist import Artist
from mixes.models.Label import Label


class Podcast(models.Model):
    """
    Mix podcasts
    """

    id = models.AutoField(primary_key=True)
    title = models.CharField(
        max_length=2048,
        help_text = 'Title',
        blank=True
    )
    source = models.CharField(
        max_length=2048,
        default='',
        help_text='(https://soundcloud.com/schubfaktor/podcast-017-mixed-by-viper-xxl)'
    )
    bpm = models.PositiveSmallIntegerField(
        verbose_name=_('bpm_admin'),
        default=0,
        blank=True
    )
    supplier = models.CharField(
        verbose_name=_('supplier_admin'),
        max_length=1024,
        default='',
        blank=True
    )
    player_id = models.CharField(
        verbose_name=_('player_id_admin'),
        max_length=2048,
        default='',
        blank=True
    )
    embed = models.TextField(
        verbose_name=_('embed_admin'),
        max_length=4096,
        blank=True
    )
    thumb = models.CharField(
        verbose_name=_('thumb_admin'),
        max_length=2048,
        default='',
        blank=True
    )
    genre = models.ManyToManyField(Genres)
    artist = models.ManyToManyField(
        Artist,
        default='',
        blank=True
    )
    podcast = models.ManyToManyField(
        PodcastCollector,
        blank=True
    )
    podcast_id = models.CharField(
        verbose_name=_('podcast_id_admin'),
        max_length=512,
        default='',
        blank=True
    )
    desc = models.CharField(
        verbose_name=_('desc_admin'),
        max_length=5192,
        default='',
        blank=True
    )
    create_date = models.DateTimeField(
        auto_now_add=True,
        blank=True
    )
    show = models.BooleanField(
        verbose_name=_('Show'),
        default=True
    )

    def __unicode__(self):
        return self.title