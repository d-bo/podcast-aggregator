from BeautifulSoup import BeautifulSoup
import urllib2
from django.template import Context, loader


class Soundcloud:

    def process(self, obj):
        """
        Get soundcloud podcast mix properties
        """
        # check if new record
        if 'http' in obj.source and not obj.title:
            # load source html page
            page = urllib2.urlopen(obj.source)
            page = page.read()
            #page = page.lower()
            soup = BeautifulSoup(page)

            # taking podcast .jpg from source page
            thumb = self.pullThumb(soup)
            thumb = str(thumb)
            obj.thumb = thumb

            # taking player ID from <meta> tag
            obj.player_id = self.pullPlayerID(soup)

            # podcast original title
            obj.title = self.pullTitle(soup)

            # podcast description
            obj.desc = self.pullDesc(soup)

            # pre-render iframe template (soundcloud.iframe.html)
            t = loader.get_template('iframes/soundcloud.iframe.html')
            c = Context({'player_id': obj.player_id})
            obj.embed = t.render(c)

        return obj


    def process_podcast_label(self, obj):
        """
        Get soundcloud podcast properties
        """
        # check if new record
        if 'http' in obj.source:
            # load source html page
            page = urllib2.urlopen(obj.source)
            page = page.read()
            #page = page.lower()
            soup = BeautifulSoup(page)

            # taking podcast .jpg from source page
            thumb = self.pullThumb(soup)
            thumb = str(thumb)
            obj.thumb = thumb

            # podcast description
            obj.desc = str(self.pullDescPodcast(soup)).replace("\n", '<br>')

        return obj


    def pullThumb(self, soup):
        """
        Pull podcast image from source html
        """
        tags = soup.findAll('img')
        match = [x['src'].encode('utf8') for x in tags if x.has_key('alt') and x.has_key('itemprop')]
        if len(match) == 1:
            return ''.join(match)
        else:
            #return match
            return 'ERROR: more than one image match criteria'


    def pullPlayerID(self, soup):
        """
        Pull player ID from source html
        """
        tags = soup.find('meta', {"property": "twitter:app:url:ipad"})
        if tags.has_key('content'):
            chunks = tags['content'].split(':')
            return chunks[2].encode('utf8')


    def pullTitle(self, soup):
        """
        Find title
        """
        tags = soup.find('meta', {'property': 'twitter:title'})
        return tags['content']


    def pullDesc(self, soup):
        tags = soup.find('meta', {'itemprop': 'description'})
        return tags['content'].replace("\n", '<br>')


    def pullDescPodcast(self, soup):
        tags = soup.find('p', {'itemprop': 'description'})
        return tags.renderContents()
        '''
        if 'contents' in tags:
            return tags.contents[0].replace("\n", '<br>')
        else:
            return ''
        '''