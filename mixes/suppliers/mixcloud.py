from BeautifulSoup import BeautifulSoup
import urllib2
from django.template import Context, loader
import urllib


class Mixcloud:

    def process(self, obj):
        """
        Get mixcloud podcast properties
        """
        # check if new record
        if 'http' in obj.source and not obj.title:
            # load source html page
            page = urllib2.urlopen(obj.source)
            page = page.read()
            #page = page.lower()
            soup = BeautifulSoup(page)

            # taking podcast .jpg from source page <img> tag (soundcloud)
            thumb = self.pullThumb(soup)
            thumb = str(thumb)
            obj.thumb = thumb
            
            # taking player ID
            obj.player_id = urllib.quote(obj.source)

            # podcast original title
            obj.title = self.pullTitle(soup)

            # podcast description
            obj.desc = self.pullDesc(soup)

            # pre-render iframe template (soundcloud.iframe.html)
            t = loader.get_template('iframes/mixcloud.iframe.html')
            c = Context({'player_id': obj.player_id})
            obj.embed = t.render(c)

        return obj


    def pullThumb(self, soup):
        """
        Pull podcast image from source html
        """
        tags = soup.find('img', {"class": "cloudcast-image"})
        if tags.has_key('src'):
            return tags['src'].encode('utf8')


    def pullTitle(self, soup):
        """
        Find title
        """
        tags = soup.find('h1', {'class': 'cloudcast-title'})
        return tags.string


    def pullDesc(self, soup):
        tags = soup.find('meta', {'property': 'og:description'})
        return tags['content'].replace("\n", '<br>')