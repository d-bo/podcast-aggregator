from BeautifulSoup import BeautifulSoup
import urllib2
from django.template import Context, loader


class Promodj:

    def process(self, obj):
        """
        Get promodj podcast properties
        """
        # check if new record
        if 'http' in obj.source and not obj.title:
            # load source html page
            page = urllib2.urlopen(obj.source)
            page = page.read()
            soup = BeautifulSoup(page)

            # taking podcast .jpg from source page <img> tag (soundcloud)
            thumb = self.pullThumb(soup)
            thumb = str(thumb)
            obj.thumb = thumb

            # taking player ID from <meta> tag
            obj.player_id = self.pullPlayerID(soup)

            # podcast original title
            obj.title = self.pullTitle(soup)

            # pre-render iframe template (soundcloud.iframe.html)
            t = loader.get_template('iframes/promodj.iframe.html')
            c = Context({'player_id': obj.player_id})
            obj.embed = t.render(c)

        return obj


    def pullThumb(self, soup):
        """
        Pull podcast image from source html
        """
        
        tags = soup.find('img', {"class": "avatar"})
        if tags.has_key('src'):
            return tags['src'].encode('utf8')


    def pullPlayerID(self, soup):
        """
        Pull player ID from source html
        """
        tags = soup.find('a', {'class': 'share share_vk'})
        if tags.has_key('onclick'):
            chunks = tags['onclick'].split(',')
            return chunks[2].strip('(;\')')
        return '!!! not found'


    def pullTitle(self, soup):
        """
        Find title
        """
        tags = soup.find('meta', {'property': 'og:title'})
        return tags['content']