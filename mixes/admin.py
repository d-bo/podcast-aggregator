import os
from django.contrib import admin
from urlparse import urlparse
import urllib2
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from django.conf import settings

from mixes.suppliers.soundcloud import Soundcloud
from mixes.suppliers.mixcloud import Mixcloud
from mixes.suppliers.promodj import Promodj

from mixes.models.Podcast import Podcast
from mixes.models.PodcastCollector import PodcastCollector
from mixes.models.Genres import Genres
from mixes.models.Video import Video
from mixes.models.Feedback import Feedback
from mixes.models.Artist import Artist
from mixes.models.SubmitPodcast import SubmitPodcast
from mixes.models.Label import Label

from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple


def detectSupplier(link):
    a = urlparse(link)
    netloc = a.netloc.split('.')
    # breaks at ex. subdomain.cloudservice.com
    a = [x for x in netloc if len(x) > 4]
    return ''.join(a)


def upload(id, url, prefix=''):
    """
    Upload image file to Google drive
    """
    
    try:
        url.index('http')
    except:
        url = 'http:' + url
    req = urllib2.urlopen(url)

    # backup to different folders
    # podcast_img, podcast_label (on GD storage)
    if len(prefix) > 0:
        file = prefix + '-' + str(id) + '.jpg'
        fid = settings.GD_UPLOAD_FOLDER_PODCAST_LABEL_IMG
    else:
        file = str(id) + '.jpg'
        fid = settings.GD_UPLOAD_FOLDER_PODCAST_IMG

    CHUNK = 16 * 1024
    with open(file, 'wb') as fp:
        while True:
            chunk = req.read(CHUNK)
            if not chunk: break
            fp.write(chunk)

    gauth = GoogleAuth()
    gauth.LocalWebserverAuth()
    drive = GoogleDrive(gauth)

    f = drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": fid}]})
    f.SetContentFile(file)
    f.Upload()

    os.remove(file)


@admin.register(Podcast)
class PodcastAdmin(admin.ModelAdmin):
    show_full_result_count=True
    search_fields = ['title']
    filter_horizontal = ('genre', 'artist', 'podcast')
    list_per_page = 20
    show_full_result_count = True
    fieldsets = (
        ('Auto collect', {
            'fields': (
                'show',
                'source',
                'bpm',
                'genre',
                'artist',
                'podcast',
                'podcast_id'
            )
        }),
        ('Collected', {
            #'classes': ('collapse',),
            'fields': (
                'title',
                'supplier',
                'player_id',
                'embed',
                'thumb',
                'desc'
                )
            })
    )
    #save_on_top=True

    def save_model(self, request, obj, form, change):
        """
        Hook model save
        """
        # detect cloud supplier
        obj.supplier = detectSupplier(obj.source)
        if 'soundcloud' in obj.supplier:
            obj = Soundcloud().process(obj)
        if 'mixcloud' in obj.supplier:
            obj = Mixcloud().process(obj)
        if 'promodj' in obj.supplier:
            obj = Promodj().process(obj)

        obj.save()
        upload(obj.id, obj.thumb)

    def detectSupplier(self, link):
        a = urlparse(link)
        netloc = a.netloc.split('.')
        # breaks at ex. subdomain.cloudservice.com
        a = [x for x in netloc if len(x) > 4]
        return ''.join(a)


@admin.register(Genres)
class GenresAdmin(admin.ModelAdmin):
    search_fields = ['name']
    #list_editable=('id', 'name')
    pass


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    search_fields = ['title']
    #list_editable=('id', 'name')
    pass


@admin.register(PodcastCollector)
class PodcastCollectorAdmin(admin.ModelAdmin):
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        """
        Hook model save
        """
        # detect cloud supplier
        obj.supplier = detectSupplier(obj.source)
        if 'soundcloud' in obj.supplier:
            obj = Soundcloud().process_podcast_label(obj)
        if 'mixcloud' in obj.supplier:
            obj = Mixcloud().process(obj)
        if 'promodj' in obj.supplier:
            obj = Promodj().process(obj)

        obj.save()
        upload(obj.id, obj.thumb, 'podcast-label')


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    search_fields = ['email']
    pass


@admin.register(SubmitPodcast)
class SubmitPodcastAdmin(admin.ModelAdmin):
    search_fields = ['link']
    pass


@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):
    search_fields = ['name']
    exclude = ('source', 'desc', 'thumb')
    pass


@admin.register(Label)
class LabelAdmin(admin.ModelAdmin):
    search_fields = ['name']
    pass
