from django.contrib.syndication.views import Feed
from mixes.models.Podcast import Podcast
from django.core.urlresolvers import reverse


class LatestEntriesFeed(Feed):

    title = "Podcast feed"
    link = "/podcasts/"
    description = "Podcast collector latest updates"

    def items(self):
        return Podcast.objects.all().order_by('-id')[:12]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.desc

    def item_link(self, item):
        return reverse('podcasts_show', args=[item.pk])

    def item_pubdate(self, item):
        return item.create_date