from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.core.urlresolvers import resolve
from django.db.models import Avg, Max, Min
import json

from mixes.models.Podcast import Podcast
from mixes.models.PodcastCollector import PodcastCollector
from mixes.models.Video import Video
from mixes.models.Genres import Genres

from mixes.forms.FeedbackForm import FeedbackForm
from mixes.forms.SubmitPodcastForm import SubmitPodcastForm


def index(request, page=1):
    '''
    Home page
    '''

    data = Podcast.objects.all().filter(show=True).order_by('-id')[:12]
    data_artists = data[:12]
    pg = Paginator(data, 12)
    podcasts = pg.page(page)

    bpm = list(Podcast.objects.values('bpm').filter(bpm__gte=1,show=True))
    bpmMaxMin = set([x['bpm'] for x in bpm])
    #bpmMaxMin = Podcast.objects.values('bpm').filter(bpm__gte=1).aggregate(Min('bpm'), Max('bpm'))

    video = Video.objects.all().order_by('-id')
    video_pg = Paginator(video, 9)
    videos = video_pg.page(1)

    request_context = RequestContext(request)
    request_context.push({
        'current_url': resolve(request.path_info).url_name,
        'data': podcasts,
        'video': videos,
        'podcast_genres': Podcast.objects.values('genre__name').distinct().exclude(genre__name=None).filter(show=True),
        'video_genres': Video.objects.values('genre__name').distinct().exclude(genre__name=None),
        'feedback_form': FeedbackForm,
        'submit_podcast': SubmitPodcastForm,
        'techno_list': Podcast.objects.filter(genre__name='techno',show=True).order_by('-id')[:5],
        'drum_list': Podcast.objects.filter(genre__name="drum'n'bass",show=True).order_by('-id')[:5],
        'electro_list': Podcast.objects.filter(genre__name="electro",show=True).order_by('-id')[:5],
        'ambient_list': Podcast.objects.filter(genre__name="ambient",show=True).order_by('-id')[:5],
        'podcast_collector': PodcastCollector.objects.all().order_by('-id')[:20],
        'data_artists': data_artists,
        'bpmMaxMin': sorted(list(bpmMaxMin)),
        #'bpmMaxMin': bpmMaxMin
    })

    #if 'HTTP_X_PJAX' in request.META:
    if request.is_ajax():
        return render_to_response('index/index.tpl.html', request_context)
    else:
        return render_to_response('index/index.html', request_context)