from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.template import RequestContext
from django.core.urlresolvers import resolve
import urllib2
from django.core.mail import send_mail
from django.middleware.csrf import rotate_token

from mixes.models.Podcast import Podcast
from mixes.models.Artist import Artist
from mixes.models.PodcastCollector import PodcastCollector
from mixes.models.Video import Video
from mixes.models.Genres import Genres

from mixes.forms.FeedbackForm import FeedbackForm
from mixes.forms.SubmitPodcastForm import SubmitPodcastForm


def referer(request):
    """
    Back button
    """
    if 'HTTP_REFERER' in request.META:
        ref = request.META['HTTP_REFERER']
    else:
        ref = '/'
    return ref


def podcasts(request, page=1):
    '''
    Podcasts view
    '''
    search = False
    podcast_label = False

    if 'tag' in request.GET:
        search = 'tag'
        tag = urllib2.unquote(request.GET['tag'])
        data = Podcast.objects.filter(show=True, genre__name=tag).order_by('-id')
    elif 'podcast' in request.GET:
        search = 'podcast'
        tag = urllib2.unquote(request.GET['tag'])
        data = Podcast.objects.all().filter(show=True).order_by('-id')
    elif 'name' in request.GET:
        search = 'name'
        tag = urllib2.unquote(request.GET['name'])
        data = Podcast.objects.filter(podcast__name=tag, show=True).order_by('-id')
        podcast_label = PodcastCollector.objects.filter(name=tag)
    elif 'bpm' in request.GET:
        search = 'bpm'
        tag = urllib2.unquote(request.GET['bpm'])
        data = Podcast.objects.filter(bpm=tag, show=True).order_by('-id')
    elif 'artist' in request.GET:
        search = 'artist'
        tag = urllib2.unquote(request.GET['artist'])
        data = Podcast.objects.filter(artist__name=tag, show=True).order_by('-id')
    else:
        data = Podcast.objects.all().filter(show=True).order_by('-id')
    pg = Paginator(data, 12)
    #page = pg.num_pages
    if 'page' in request.GET:
        page = request.GET['page']
    podcasts = pg.page(page)

    request_context = RequestContext(request)
    request_context.push({
        'data': podcasts,
        'feedback_form': FeedbackForm,
        'submit_podcast': SubmitPodcastForm,
        'current_url': resolve(request.path_info).url_name,
        'genres': Podcast.objects.values('genre__name').distinct().exclude(genre__name=None),
        'bpm': Podcast.objects.values('bpm').distinct().order_by('bpm'),
        'podcasts': PodcastCollector.objects.all(),
        'techno_list': Podcast.objects.filter(genre__name='techno',show=True).order_by('-id')[:5],
        'drum_list': Podcast.objects.filter(genre__name="drum'n'bass",show=True).order_by('-id')[:5],
        'electro_list': Podcast.objects.filter(genre__name="electro",show=True).order_by('-id')[:5],
        'ambient_list': Podcast.objects.filter(genre__name="ambient",show=True).order_by('-id')[:5],
        'search': request.GET[search] if search in request.GET else False,
        'back': referer(request),
        'podcast_label': podcast_label if podcast_label else False,
    })

    #if 'HTTP_X_PJAX' in request.META:
    if request.is_ajax():
        return render_to_response('podcasts/podcasts.tpl.html', request_context)
    else:
        return render_to_response('podcasts/podcasts.html', request_context)


def podcasts_show(request, podcast_id):

    try:
        podcast = Podcast.objects.get(pk=podcast_id)
    except:
        podcast = False

    # podcast ids index
    podcast_ids = []
    podcast_id_index = list(Podcast.objects.all().values('id'))
    for e in podcast_id_index:
        podcast_ids.append(int(e['id']))
    podcast_item = podcast_ids.index(int(podcast_id))
    try:
        prev_podcast = podcast_ids[podcast_item + 1]
    except:
        prev_podcast = podcast_ids[podcast_item]
    next_podcast = podcast_ids[podcast_item - 1]

    #podcast_id_index = podcast_id_index.index(podcast_id)

    request_context = RequestContext(request)
    request_context.push({
        'data': podcast_id,
        'feedback_form': FeedbackForm,
        'submit_podcast': SubmitPodcastForm,
        'current_url': resolve(request.path_info).url_name,
        'genres': Podcast.objects.values('genre__name').distinct().exclude(genre__name=None),
        'cast': podcast,
        'adviced': Podcast.objects.all().order_by('-id')[:6],
        'latest_podcast': Podcast.objects.latest('id'),
        'techno_list': Podcast.objects.filter(genre__name='techno',show=True).order_by('-id')[:6],
        'drum_list': Podcast.objects.filter(genre__name="drum'n'bass",show=True).order_by('-id')[:6],
        'electro_list': Podcast.objects.filter(genre__name="electro",show=True).order_by('-id')[:6],
        'ambient_list': Podcast.objects.filter(genre__name="ambient",show=True).order_by('-id')[:6],
        'meta_title': podcast.title,
        'back': referer(request),
        'next_podcast': next_podcast,
        'prev_podcast': prev_podcast,
    })

    #if 'HTTP_X_PJAX' in request.META:
    if request.is_ajax():
        return render_to_response('podcasts/podcasts-show.tpl.html', request_context)
    else:
        return render_to_response('podcasts/podcasts-show.html', request_context)


def podcasts_directory(request):

    data = PodcastCollector.objects.all()
    row_breaks = [x*4 for x in xrange(100)]
    tag = 'name'

    out = {}
    for k in data:
        index = k.name[:1].upper()
        if index.isnumeric():
            index = 0
        if not index in out:
            out[index] = []
        out[index].append(k.name)

    request_context = RequestContext(request)
    request_context.push({
        'rb': row_breaks,
        'data': out,
        'tag': tag,
        'feedback_form': FeedbackForm,
        'submit_podcast': SubmitPodcastForm,
        'current_url': resolve(request.path_info).url_name,
        'techno_list': Podcast.objects.filter(genre__name='techno',show=True).order_by('-id')[:5],
        'drum_list': Podcast.objects.filter(genre__name="drum'n'bass",show=True).order_by('-id')[:5],
        'electro_list': Podcast.objects.filter(genre__name="electro",show=True).order_by('-id')[:5],
        'ambient_list': Podcast.objects.filter(genre__name="ambient",show=True).order_by('-id')[:5],
    })

    #if 'HTTP_X_PJAX' in request.META:
    if request.is_ajax():
        return render_to_response('podcasts/podcasts_directory.tpl.html', request_context)
    else:
        return render_to_response('podcasts/podcasts_directory.html', request_context)


def podcasts_artists(request):

    data = Artist.objects.all()
    row_breaks = [x*4 for x in xrange(100)]
    tag = 'artist'

    out = {}
    for k in data:
        index = k.name[:1].upper()
        if index.isnumeric():
            index = 0
        if not index in out:
            out[index] = []
        out[index].append(k.name)

    request_context = RequestContext(request)
    request_context.push({
        'rb': row_breaks,
        'data': out,
        'tag': tag,
        'feedback_form': FeedbackForm,
        'submit_podcast': SubmitPodcastForm,
        'current_url': resolve(request.path_info).url_name,
        'techno_list': Podcast.objects.filter(genre__name='techno',show=True).order_by('-id')[:5],
        'drum_list': Podcast.objects.filter(genre__name="drum'n'bass",show=True).order_by('-id')[:5],
        'electro_list': Podcast.objects.filter(genre__name="electro",show=True).order_by('-id')[:5],
        'ambient_list': Podcast.objects.filter(genre__name="ambient",show=True).order_by('-id')[:5],
    })

    #if 'HTTP_X_PJAX' in request.META:
    if request.is_ajax():
        return render_to_response('podcasts/podcasts_directory.tpl.html', request_context)
    else:
        return render_to_response('podcasts/podcasts_directory.html', request_context)