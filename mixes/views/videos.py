from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.template import RequestContext
from django.core.urlresolvers import resolve
import urllib
from django.core.mail import send_mail
from django.middleware.csrf import rotate_token

from mixes.models.Podcast import Podcast
from mixes.models.PodcastCollector import PodcastCollector
from mixes.models.Video import Video
from mixes.models.Genres import Genres

from mixes.forms.FeedbackForm import FeedbackForm
from mixes.forms.SubmitPodcastForm import SubmitPodcastForm


def videos(request, page=1):
    '''
    Videos view
    '''

    if 'tag' in request.GET:
        tag = urllib.unquote(request.GET['tag'])
        data = Video.objects.filter(genre__name=tag).order_by('-id')
    else:
        data = Video.objects.all().order_by('-id')
    pg = Paginator(data, 10)
    #page = pg.num_pages
    if 'page' in request.GET:
        page = request.GET['page']
    podcasts = pg.page(page)
   
    request_context = RequestContext(request)
    request_context.push({
        'data': podcasts,
        'feedback_form': FeedbackForm,
        'current_url': resolve(request.path_info).url_name,
        'genres': Video.objects.values('genre__name').distinct().exclude(genre__name=None),
        'techno_list': Podcast.objects.filter(genre__name='techno').order_by('-id')[:5],
        'drum_list': Podcast.objects.filter(genre__name="drum'n'bass").order_by('-id')[:5],
        'electro_list': Podcast.objects.filter(genre__name="electro").order_by('-id')[:5],
        'ambient_list': Podcast.objects.filter(genre__name="ambient").order_by('-id')[:5],
    })

    #if 'HTTP_X_PJAX' in request.META:
    if request.is_ajax():
        return render_to_response('videos/videos.tpl.html', request_context)
    else:
        return render_to_response('videos/videos.html', request_context)