from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.template import RequestContext
from django.core.urlresolvers import resolve
import urllib
from django.core.mail import send_mail
from django.middleware.csrf import rotate_token

from mixes.models.Podcast import Podcast
from mixes.models.PodcastCollector import PodcastCollector
from mixes.models.Video import Video
from mixes.models.Genres import Genres

from mixes.forms.FeedbackForm import FeedbackForm
from mixes.forms.SubmitPodcastForm import SubmitPodcastForm


def submit_podcast(request):
    if request.method == 'POST':
        form = SubmitPodcastForm(request.POST)
        if form.is_valid():
            link = form.cleaned_data['link']
            send_mail(
                'Message from site', 
                "Link: \n" + link, 
                'info@timeavto66.ru', 
                ['abelt@e1.ru'], 
                fail_silently=False
            )
            form.save()
        return render_to_response('feedback.html')
    else:
        return redirect('/')