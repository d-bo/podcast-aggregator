from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.template import RequestContext
from django.core.urlresolvers import resolve
import urllib
from django.core.mail import send_mail
from django.middleware.csrf import rotate_token

from mixes.models.Podcast import Podcast
from mixes.models.PodcastCollector import PodcastCollector
from mixes.models.Video import Video
from mixes.models.Genres import Genres

from mixes.forms.FeedbackForm import FeedbackForm
from mixes.forms.SubmitPodcastForm import SubmitPodcastForm


def feedback(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            desc = form.cleaned_data['desc']
            send_mail(
                'Message from site', 
                "From: \n" + email + "\n\nMessage: \n" + desc, 
                'info@timeavto66.ru', 
                ['abelt@e1.ru'], 
                fail_silently=False
            )
            form.save()
        rotate_token(request)
        return render_to_response('feedback.html')
    else:
        return redirect('/')