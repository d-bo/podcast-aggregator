from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.template import RequestContext
from django.core.urlresolvers import resolve
import urllib
from django.core.mail import send_mail
from django.middleware.csrf import rotate_token

from mixes.models.Podcast import Podcast
from mixes.models.PodcastCollector import PodcastCollector
from mixes.models.Video import Video
from mixes.models.Genres import Genres

from mixes.forms.FeedbackForm import FeedbackForm
from mixes.forms.SubmitPodcastForm import SubmitPodcastForm


def videos_show(request, video_id):

    try:
        video = Video.objects.get(pk=video_id)
    except:
        video = False

    request_context = RequestContext(request)
    request_context.push({
        'data': video_id,
        'feedback_form': FeedbackForm,
        'current_url': resolve(request.path_info).url_name,
        'genres': Video.objects.values('genre__name').distinct().exclude(genre__name=None),
        'cast': video,
        'adviced': Video.objects.all().order_by('-id')[:6],
        'video_model': Video,
        'latest_video': Video.objects.latest('id')
    })

    #if 'HTTP_X_PJAX' in request.META:
    if request.is_ajax():
        return render_to_response('videos/videos-show.tpl.html', request_context)
    else:
        return render_to_response('videos/videos-show.html', request_context)