from django.http import HttpResponse
from django.core import serializers

from mixes.models.Podcast import Podcast

def get(request, page=1):
    '''
    api return podcasts
    '''

    data = Podcast.objects.all().filter(show=True).order_by('-id')[:12]
    data = serializers.serialize('json', data)
    return HttpResponse(data, content_type='application/json')