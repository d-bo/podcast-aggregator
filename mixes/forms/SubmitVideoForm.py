from django.forms import ModelForm, Textarea, TextInput
from mixes.models.Feedback import Feedback
from mixes.models.SubmitPodcast import SubmitPodcast
from mixes.models.SubmitVideo import SubmitVideo


class SubmitVideoForm(ModelForm):
	class Meta:
		model=SubmitPodcast
		fields=['link']
		widgets={
			'link': TextInput(attrs={'class': 'custom-input'}),
		}