from django.forms import ModelForm, Textarea, TextInput
from mixes.models.Feedback import Feedback
from mixes.models.SubmitPodcast import SubmitPodcast
from mixes.models.SubmitVideo import SubmitVideo

class FeedbackForm(ModelForm):
	class Meta:
		model=Feedback
		fields=['email', 'desc']
		widgets={
			'desc': Textarea(attrs={'cols': 12, 'rows': 3, 'class': 'custom-input'}),
			'email': TextInput({'class': 'custom-input'}),
		}