# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mixes', '0002_auto_20150819_0958'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='podcast',
            name='label',
        ),
    ]
