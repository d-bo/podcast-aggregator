# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mixes', '0004_podcast_show'),
    ]

    operations = [
        migrations.AlterField(
            model_name='podcast',
            name='show',
            field=models.BooleanField(default=True, verbose_name='Show'),
        ),
    ]
