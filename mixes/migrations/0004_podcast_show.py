# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mixes', '0003_remove_podcast_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='podcast',
            name='show',
            field=models.BooleanField(default=True),
        ),
    ]
