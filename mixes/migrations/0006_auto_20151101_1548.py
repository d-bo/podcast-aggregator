# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mixes', '0005_auto_20151101_1534'),
    ]

    operations = [
        migrations.AlterField(
            model_name='podcast',
            name='bpm',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='bpm_admin', blank=True),
        ),
        migrations.AlterField(
            model_name='podcast',
            name='desc',
            field=models.CharField(default=b'', max_length=5192, verbose_name='desc_admin', blank=True),
        ),
        migrations.AlterField(
            model_name='podcast',
            name='embed',
            field=models.TextField(max_length=4096, verbose_name='embed_admin', blank=True),
        ),
        migrations.AlterField(
            model_name='podcast',
            name='player_id',
            field=models.CharField(default=b'', max_length=2048, verbose_name='player_id_admin', blank=True),
        ),
        migrations.AlterField(
            model_name='podcast',
            name='podcast_id',
            field=models.CharField(default=b'', max_length=512, verbose_name='podcast_id_admin', blank=True),
        ),
        migrations.AlterField(
            model_name='podcast',
            name='supplier',
            field=models.CharField(default=b'', max_length=1024, verbose_name='supplier_admin', blank=True),
        ),
        migrations.AlterField(
            model_name='podcast',
            name='thumb',
            field=models.CharField(default=b'', max_length=2048, verbose_name='thumb_admin', blank=True),
        ),
    ]
