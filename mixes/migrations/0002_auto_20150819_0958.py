# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mixes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artist',
            name='source',
            field=models.CharField(default=b'', help_text=b'(https://soundcloud.com/neinrecords)', max_length=2048, blank=True),
        ),
    ]
