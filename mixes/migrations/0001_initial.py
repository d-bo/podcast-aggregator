# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Artist',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('source', models.CharField(default=b'', help_text=b'(https://soundcloud.com/neinrecords)', max_length=2048)),
                ('name', models.CharField(default=b'', max_length=2048, blank=True)),
                ('desc', models.CharField(default=b'', max_length=5192, blank=True)),
                ('thumb', models.CharField(default=b'', max_length=2048, blank=True)),
                ('create_date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('email', models.EmailField(max_length=512, verbose_name='Your email')),
                ('desc', models.CharField(max_length=2048, verbose_name='Text')),
            ],
        ),
        migrations.CreateModel(
            name='Genres',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=2048, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Label',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=2048, blank=True)),
                ('desc', models.CharField(default=b'', max_length=5192, blank=True)),
                ('img', models.CharField(default=b'', max_length=2048, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Podcast',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(help_text=b'Title', max_length=2048, blank=True)),
                ('source', models.CharField(default=b'', help_text=b'(https://soundcloud.com/schubfaktor/podcast-017-mixed-by-viper-xxl)', max_length=2048)),
                ('bpm', models.PositiveSmallIntegerField(default=0, blank=True)),
                ('supplier', models.CharField(default=b'', max_length=1024, blank=True)),
                ('player_id', models.CharField(default=b'', max_length=2048, blank=True)),
                ('embed', models.TextField(max_length=4096, blank=True)),
                ('thumb', models.CharField(default=b'', max_length=2048, blank=True)),
                ('podcast_id', models.CharField(default=b'', max_length=512, blank=True)),
                ('desc', models.CharField(default=b'', max_length=5192, blank=True)),
                ('create_date', models.DateTimeField(auto_now_add=True)),
                ('artist', models.ManyToManyField(default=b'', to='mixes.Artist', blank=True)),
                ('genre', models.ManyToManyField(to='mixes.Genres')),
                ('label', models.ManyToManyField(default=b'', to='mixes.Label', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='PodcastCollector',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('source', models.CharField(default=b'', help_text=b'(https://soundcloud.com/neinrecords)', max_length=2048)),
                ('name', models.CharField(default=b'', max_length=2048, blank=True)),
                ('desc', models.CharField(default=b'', max_length=5192, blank=True)),
                ('thumb', models.CharField(default=b'', max_length=2048, blank=True)),
                ('create_date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='SubmitPodcast',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('link', models.CharField(max_length=2048, verbose_name=b'')),
            ],
        ),
        migrations.CreateModel(
            name='SubmitVideo',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('link', models.CharField(max_length=2048, verbose_name=b'')),
            ],
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(help_text=b'Title', max_length=2048, blank=True)),
                ('embed', models.CharField(default=b'', help_text=b'(http://www.youtube.com/watch?v=5YYTEp4PFXw)', max_length=2048)),
                ('embed_id', models.CharField(default=b'', help_text=b'(youtube id: JIPZruqrZ5w)', max_length=1024)),
                ('create_date', models.DateTimeField(auto_now_add=True)),
                ('genre', models.ManyToManyField(to='mixes.Genres', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='podcast',
            name='podcast',
            field=models.ManyToManyField(to='mixes.PodcastCollector', blank=True),
        ),
    ]
