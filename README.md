# podcast-aggregator

## Install

```bash
virtualenv podcast
source podcast/bin/activate
cd podcast
git clone https://bitbucket.org/d-bo/podcast-aggregator.git
cd podcast-aggregator

sudo apt-get install python-dev

# MYSQL
sudo apt-get install libmysqlclient-dev

# POSTGRESQL
sudo apt-get install libpq-dev

pip install -r requirements.txt
npm install
bower install --allow-root
# import fixtures
# CREATE DATABASE podcast CHARACTER SET utf8 COLLATE utf8_general_ci;
./manage.py loaddata dump/dump.data
# or u can do it from mysql console `source dump/podcast-latest.sql`
```



## Google Drive API prepare

Create project at https://console.developers.google.com/

Follow the steps to create client ID and client secret https://developers.google.com/drive/web/auth/web-server

Download client_secret.json from Google Developers Console and put in the project root. Set OAUTH callback domain also. It must contain working domain (http://localhost:8080/ as on local machine)

You can get folder ID by running ```listgoogledrive``` command from the project root folder:

```bash
python manage.py listgoogledrive
```

Get folder ID where you want to upload files from the output.

Set folder ID in webapp/settings.py:

```
...
GOOGLE_DRIVE_UPLOAD_FOLDER_ID = 'xxx'
...
```



## Social auth

### Twitter

Create application at https://apps.twitter.com/

### Soundcloud

Set redirect_uri to ```http://127.0.0.1:8000/complete/soundcloud/```
Notice */complete/soundcloud/*

### Google

Set redirect_uri to ```http://localhost:5000/complete/google-oauth2/```
Notice */complete/google-oauth2/*



## Backup shortcuts

```mysql
mysqldump -u root -p12345 --no-create-db --no-create-info --skip-add-locks --skip-extended-insert --complete-insert podcast > dump/podcast-latest.sql
```



## PostgreSQL notes

### Create db shortcuts

```
sudo -u postgres psql postgres
postgres=# CREATE ROLE user WITH LOGIN PASSWORD '12345';
postgres=# CREATE DATABASE podcast ENCODING 'UTF-8'
postgres=# grant all privileges on database podcast to user;
```

### Migrations

Avoid ```ProgrammingError: relation "auth_user" does not exist```

```
# first run sites, auth migration
# dont look at the warnings
python manage.py migrate sites
python manage.py migrate auth
python manage.py migrate
```

### Migrate from MySQL

Export from MySQL

```bash
python manage.py dumpdata mixes > dump/django_data.json
```

Change settings.py to postgresql connection.

Make migrations as in ```Migrations``` chapter.

Import data

```bash
python manage.py loaddata mixes dump/django_data.json
```

## todo

Frontend migrate to:

* webpack
* backbone marionette ? ember? angular? knockout?
* babel
* karma? jasmine ?
* phantomjs (seo)

Backend:

* django cachalot in production
* api for front-end