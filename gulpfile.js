'use strict';

var gulp = require('gulp'),
  rename = require('gulp-rename'),
  uglify = require('gulp-uglify'),
  minifyCSS = require('gulp-minify-css'),
  clean = require('gulp-clean'),
  concat = require('gulp-concat');

var DEST = 'webapp/static/build/';

gulp.task('clean', function () {
    return gulp.src('static/build/*', {read: false})
        .pipe(clean());
});

gulp.task('minify-js', function() {
  return gulp.src([
      'webapp/static/vendor/jquery/jquery.min.js',
      'webapp/static/vendor/moment/min/moment.min.js',
      //'static/vendor/bootstrap2.3.2/js/bootstrap-button.js',
      //'static/vendor/bootstrap2.3.2/js/bootstrap-dropdown.js',
      //'static/vendor/bootstrap2.3.2/js/bootstrap-collapse.js',
      //'static/vendor/bootstrap2.3.2/js/bootstrap-tooltip.js',
      //'static/vendor/bootstrap2.3.2/js/bootstrap-popover.js',
      //'static/vendor/fullcalendar/dist/fullcalendar.min.js',
      'webapp/static/vendor/jquery-plainmodal/jquery.plainmodal.min.js',
      'webapp/static/vendor/nouislider/distribute/nouislider.js',
    ])
    .pipe(concat('all.js'))
    .pipe(uglify({"mangle": false}))
    .pipe(gulp.dest(DEST));
});

gulp.task('minify-css', function() {
  return gulp.src([
      'webapp/static/vendor/bootstrap2.3.2/bootstrap/css/bootstrap.css',
      'webapp/static/vendor/bootstrap2.3.2/bootstrap/css/bootstrap-responsive.css',
      //'static/css/bootstrap-responsive.css',
      //'webapp/static/vendor/fullcalendar/dist/fullcalendar.min.css',
      'webapp/static/vendor/nouislider/distribute/nouislider.min.css',
      //'static/css/*.css'
    ])
    .pipe(concat('all.css'))
    .pipe(minifyCSS({"keepSpecialComments": 0}))
    .pipe(gulp.dest(DEST))
});

gulp.task('default', function() {
    gulp.run('clean', 'minify-js', 'minify-css')
});