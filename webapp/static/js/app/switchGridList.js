
/**
 * Grid / list switch handler
 */
define(['jquery', 'backbone'], function($) {

	'use strict';

	var api_url = '/api/v1/podcast/get';

	var Route = Backbone.Router.extend({
		routes: {
			'*path': indexRoute
		}
	});

	var Model = Backbone.Model.extend({
		podcasts: [],
		url: api_url,
		parse: function(response, xhr) {
			var i = response.length, out = [];
			while (i) {
				var k = i-1;
				out[k] = {};
				out[k].title = response[k].fields.title;
				out[k].thumb = response[k].fields.thumb;
				out[k].id = response[k].pk;
				i--;
			};
			return {
				podcasts: out
			};
		}
	});
	var View = Backbone.View.extend({
		el: '#main-podcast-container',
		template: _.template($("script.template").html()),
		initialize: function() {
			//console.log(this.model.attributes.podcasts)
		},
		render: function() {
			this.$el.html(this.template({
				podcasts: this.model.attributes.podcasts
			}));
			return this;
		}
	});

	function indexRoute() {
		var m = new Model;
		m.fetch({
			success: function() {
				var v = new View({model: m});
				v.render();
			}
		});
	}

	function switchGrid(data) {
		console.log(data);
	}

	function switchList(data) {
		console.log(data);
	}

	return {
		switchPanel: switchPanel
	};
});