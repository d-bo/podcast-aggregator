define(function() {
    
    'use strict';

    return {
        iframe: function(id) {
            return {
                mixcloud: '<iframe src="https://www.mixcloud.com/widget/iframe/?autoplay=1&embed_type=widget_standard&amp;embed_uuid=3b80171e-7c82-45bd-9193-8824a2020501&amp;feed='+id+'&amp;hide_cover=1&amp;hide_tracklist=1&amp;light=1&amp;replace=0" frameborder="0" height="180" width="100%"></iframe>',
                soundcloud: '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/'+id+'&amp;color=ff5500&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>',
                promodj: '<iframe src="http://promodj.com/embed/'+id+'/big" width="100%" height="70" style="width: 100%" frameborder="0" allowfullscreen></iframe>'
            }
        },
        /**
         * open video modal
         */
        openVideo: function(id) {
            $('#player').html('');
            var modalB = $('#sample-13b').plainModal({effect: this.effect, duration: 400});
            modalB.on('plainmodalopen', function(event) {
            });
            modalB.plainModal('open');
            $('#sample-13b-content').html('<iframe width="100%" height="415" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allowfullscreen></iframe>');
        },
        /**
         * open podcast
         */
        openPodcast: function(id, supplier) {
            var podcast = this.iframe(id);
            var player = $('#player'), closeBtn = $('#player-close');
            closeBtn.css('display', 'block');
            player.fadeIn(300, function() {
                $(this).html(podcast[supplier]);
                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            });
        },
        /**
         * dirty validate feedback form
         */
        validateFeedback: function() {
            var desc = $("textarea[name=desc]").val(),
                email = $("input[name=email]").val(),
                match = 0;

            if (email == undefined || email == '') {
                $('[name=email]').css({background: 'brown', color: '#fff'});
                match++;
            } else {
                $('[name=email]').css({background: 'green', color: '#fff'});
            }

            if (desc == undefined || desc == '') {
                $('[name=desc]').css({background: 'brown', color: '#fff'});
                match++;
            } else {
                $('[name=desc]').css({background: 'green', color: '#fff'});
            }

            if (match > 0) {
                return false;
            } else {
                return true;
            }
        },
        /**
         * dirty validate submit podcast form
         */
        validateSubmitPodcast: function(form) {
            var link = $("input[name=link]").val(),
                match = 0;

            if (link == undefined || link == '') {
                $('[name=link]').css({background: 'brown', color: '#fff'});
                match++;
            } else {
                $('[name=link]').css({background: 'green', color: '#fff'});
            }

            if (match > 0) {
                return false;
            } else {
                return true;
            }
        },
        supportsSVG: function() {
            return !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', "svg").createSVGRect;
        }
    }
});
