
/**
 * Home page model
 */
define(['backbone'], function(Backbone) {

	var api_url = '/api/v1/podcast/get';

	var Model = Backbone.Model.extend({
		podcasts: [],
		url: api_url,
		parse: function(response, xhr) {
			var i = response.length, out = [];
			while (i) {
				var k = i-1;
				out[k] = {};
				out[k].title = response[k].fields.title;
				out[k].thumb = response[k].fields.thumb;
				out[k].id = response[k].pk;
				i--;
			};
			return {
				podcasts: out
			};
		}
	});
	
	return Model;
});