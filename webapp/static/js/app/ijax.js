/**
 * Simple dynamic content reloading
 *
 * pushState + ajax or a native page reload
 * backend must catch xmlhttprequest
 *
 * mark links you want to disable dynamic behavior like:
 * <a href="..." ijax="false"></a>
 *
 * @todo Separate loading progress
 */

define(['jquery'], function($) {

	'use strict';

	function ijax(params) {

		var loading = $('.loading');

		function load(params) {
			$.get(params.url, {'cacheState': $.now()})
			.done(function(data) {
				loading.fadeOut(200, function() {
					loading.css('display', 'none');
				});
				$('#dynamic-container').html(data);
				if (params.afterLoad) {
					params.afterLoad();
				}
			});
		}

		var params = params || {};
		
		if (params.url) {
			load(params);
			return false;
		}
		// clear previous events
		var all = $('a[ijax!="false"]');
		all.off();
		// bind events to 'a' links
		all.click(function(e) {
			if (history.pushState) {
				loading.fadeIn(200);
				var url = $(this).attr('href');
				params.url = url;
				load(params);
				history.pushState(null, null, url);
				return false;
			} else {
				return true;
			}
		});
	}

	return ijax;
});