
/**
 * Home page controller
 */
define(['jquery', 'backbone', 'indexModel', 'indexView'], 
	function($, Backbone, indexModel, indexView) {

		function init() {
			var m = new indexModel;
			m.fetch({
				success: function() {
					var v = new indexView({model: m});
					v.render();
				}
			});
		}

		return init;
});