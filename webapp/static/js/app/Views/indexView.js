
/**
 * Home page view
 */
define(['backbone'], function() {

	var View = Backbone.View.extend({
		el: '#main-podcast-container',
		template: _.template($("script.template").html()),
		initialize: function() {
			//console.log(this.model.attributes.podcasts)
		},
		render: function() {
			this.$el.html(this.template({
				podcasts: this.model.attributes.podcasts
			}));
			return this;
		}
	});

	return View;
});
