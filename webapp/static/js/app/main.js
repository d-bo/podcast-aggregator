'use strict';

/**
 *
 */
require([
	'jquery',
	'indexController'
	], function($, indexController) {

		'use strict';

		$(document).ready(function() {
			//application.init();

			var Route = Backbone.Router.extend({
				routes: {
					'*actions': 'indexRoute'
				}
			});			

			var route = new Route;
			route.on('route:indexRoute', indexController);
			Backbone.history.start();

		});

});
