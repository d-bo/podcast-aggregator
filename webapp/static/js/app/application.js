define(['jquery', 'ijax', 'noUiSlider', 'utils', 'switchGridList'], 
	function($, ijax, noUiSlider, Utils, switchGridList) {

	'use strict';

	var bootstrap = {};

	/**
	 * Init focus relocate
	 */
	bootstrap.initFocus = function() {
		var focus = $('#focus');
		if (focus.length) {
			//window.location.hash = '#focus';
			$('html, body').animate({
				scrollTop: focus.offset().top
			}, 500);
		}
	}

	/** 
	 * Init play button
	 */
	bootstrap.initPlayButton = function() {
		var playPodcast = $('#button-play-podcast');
		if (playPodcast.length) {
			playPodcast.on('click', function() {
				Utils.openPodcast(playPodcast.attr('data-id'), playPodcast.attr('data-supplier'));
			});
		}
	}

	/**
	 * Init close player button
	 */
	bootstrap.initClosePodcast = function() {
		var closePodcast = $('#player-close');
		if (closePodcast.length) {
			closePodcast.on('click', function(e) {
				var player = $('#player');
				player.fadeOut(300, function() {
					$(this).html('');
					closePodcast.css('display', 'none');
				});
			});
		}
	}

	/**
	 * Init submit podcast validation
	 */
	bootstrap.initValidateSubmitPodcast = function() {
		var submitPodcastForm = $('#form-submit-podcast');
		if (submitPodcastForm.length) {
			submitPodcastForm.submit(function(e) {
				var valid = Utils.validateSubmitPodcast(submitPodcastForm);
				if (!valid) {
					return false;
				}
			});
		}
	}

	/**
	 * Init slider bpm
	 */
	bootstrap.initSliderBpm = function() {
		var slider = document.getElementById('slider');
		if (slider) {

			var filterLength = window.bpm;
			var rangers = {};

			for (var i = 0; i < filterLength.length; ++i) {
				var prozent = Math.ceil((100/filterLength.length) * i);
				if (i == 0) {
					rangers['min'] = filterLength[i];
				}
				if (i < filterLength.length-1) {
					rangers[prozent+'%'] = filterLength[i];
				}
				if (i == filterLength.length-1) {
					rangers['max'] = filterLength[filterLength.length-1];
				}
			}

			noUiSlider.create(slider, {
				start: [104],
				range: rangers,
				pips: {
					mode: 'positions',
					values: [104, 110, 120, 130, 180],
					density: 2,
					stepped: true
				},
				snap: true,
				format: {
				  to: function (value) {
					return value;
				  },
				  from: function (value) {
					return value;
				  }
				}
			});

			var rangeSliderValueElement = document.getElementById('slider-value');
			slider.noUiSlider.on('update', function(values, handle) {
				rangeSliderValueElement.innerHTML = values[handle];
			});
		}
	}

	/**
	 * Init bpm search button
	 */
	bootstrap.initBpmSearchButton = function() {
		var bpmSearch = $('#btn-bpm-search');
		if (bpmSearch.length) {
			bpmSearch.on('click', function() {
				var bpm = $('#slider-value').html();
				window.location.href = '/podcasts/?bpm=' + bpm;
			});
		}
	}

	/**
	 * Init disqus
	 */
	bootstrap.initDisqus = function() {
		var dc = $('#disqus-comments');
		if (dc.length) {
			if (typeof(DISQUS) == 'object') {
				DISQUS.reset({
				  reload: true,
				  config: function () {  
				    this.page.identifier = window.location.pathname;
				    this.page.url = window.location.href;
				  }
				});
			}
		}
	}

	/**
	 * Init grid/list switch buttons
	 */
	bootstrap.initGridList = function() {
		var btn = $('#main-grid');
		btn.on('click', function() {
			switchGridList.switchPanel('grid');
		});
		var btn = $('#main-list');
		btn.on('click', function() {
			switchGridList.switchPanel('list');
		});
	}

	function init() {

		bootstrap.initFocus();
		bootstrap.initPlayButton();
		bootstrap.initClosePodcast();
		bootstrap.initValidateSubmitPodcast();
		bootstrap.initSliderBpm();
		bootstrap.initBpmSearchButton();
		bootstrap.initGridList();

		/**
		 * History back button
		 */
		 /*
		window.onpopstate = function() {
			ijax({
				url: document.location.href,
				afterLoad: init
			});
		}
		var cont = $('#history-back-container');
	    if (cont.length) {
	      if (history.pushState) {
	        cont.css('display', 'block');
	        $('#history-back-button').on('click', function() {
	          window.history.back();
	        });
	      }
	    }

		ijax({
			afterLoad: init
		});
		*/
	}

	return {
		init: init
	};
});