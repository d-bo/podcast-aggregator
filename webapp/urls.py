from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from registration.forms import RegistrationFormUniqueEmail
from registration.backends.default.views import RegistrationView

from mixes import rss
from mixes.views import auth as auth_custom_view
from mixes import views as mixes_views
from mixes.views import index as index_view
from mixes.views import podcasts as podcasts_view
from mixes.views import videos as videos_view
from mixes.views import videos_show as videos_show_view
from mixes.views import feedback as feedback_view
from mixes.views import submit_podcast as submit_podcast_view

from mixes.api.v1 import podcasts as api_v1_podcasts

urlpatterns = [
    url(r'^$', index_view.index, name='index'),

    # podcasts
    url(r'^podcasts/$', podcasts_view.podcasts, name='podcasts'),
    url(r'^podcasts/directory/$', podcasts_view.podcasts_directory, name='podcasts_directory'),
    url(r'^podcasts/artists/$', podcasts_view.podcasts_artists, name='podcasts_artists'),
    #url(r'^podcasts/(?P<page>[0-9]+)/$', mixes_views.podcasts, name='podcasts'),
    url(r'podcasts/([0-9]+)/$', podcasts_view.podcasts_show, name='podcasts_show'),

    # videos
    url(r'^videos/$', videos_view.videos, name='videos'),
    url(r'videos/(.*)/$', videos_show_view.videos_show, name='videos_show'),

    # send feedback
    url(r'^feedback/$', feedback_view.feedback, name='feedback'),
    url(r'^submit-podcast/$', submit_podcast_view.submit_podcast, name='submit_podcast'),

    # admin
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # register / auth
    url(r'^accounts/login/$', auth_views.login),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout',
            {'next_page': '/accounts/logged-out'}
        ),
    url(r'^accounts/profile/$', auth_custom_view.profile),
    url(r'^accounts/register/$', 
        RegistrationView.as_view(form_class=RegistrationFormUniqueEmail), 
        name='registration_register'
    ),
    url(r'^accounts/logged-out/$', auth_custom_view.logged_out),
    url(r'^accounts/', include('registration.backends.default.urls')),

    # RSS
    url(r'^rss/$', rss.LatestEntriesFeed(), name='rss'),

    # Social auth
    url('', include('social.apps.django_app.urls', namespace='social')),

    # api v1
    url(r'^api/v1/podcast/get$', api_v1_podcasts.get),
]

# django-registration-default templates
# https://github.com/yourcelf/django-registration-defaults
