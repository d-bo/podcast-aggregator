import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = '&mu#p3_-mwaixb^$)3%!tc2hjr4747d31qtru-aas7fi2p^z$i'


DEBUG = True
ALLOWED_HOSTS = ['*']

# Registration
ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = False
INCLUDE_AUTH_URLS = True
INCLUDE_REGISTER_URL = True
REGISTRATION_OPEN = True

DEFAULT_FROM_EMAIL = os.environ.get('PODCAST_EMAIL_FROM'),
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = os.environ.get('PODCAST_EMAIL_HOST'),
EMAIL_PORT = os.environ.get('PODCAST_EMAIL_PORT'),
#EMAIL_USE_TLS = True
EMAIL_HOST_USER = os.environ.get('PODCAST_EMAIL_USER'),
EMAIL_HOST_PASSWORD = os.environ.get('PODCAST_EMAIL_PASSWORD'),

INSTALLED_APPS = (
    #'django_admin_bootstrapped',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django_whoshere',
    'debug_toolbar',
    'mixes',
    'registration',
    'django_gravatar',
    #'cachalot',
    'social.apps.django_app.default',
    'imagestore',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django_whoshere.middleware.TrackMiddleware',
)

AUTHENTICATION_BACKENDS = (
    #'social.backends.open_id.OpenIdAuth',
    #'social.backends.google.GoogleOpenId',
    'social.backends.google.GoogleOAuth2',
    #'social.backends.google.GoogleOAuth',
    'social.backends.twitter.TwitterOAuth',
    #'social.backends.yahoo.YahooOpenId',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.soundcloud.SoundcloudOAuth2',
    'social.backends.mixcloud.MixcloudOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

ROOT_URLCONF = 'webapp.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.request",
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
)

WSGI_APPLICATION = 'webapp.wsgi.application'

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.mysql',
        'ENGINE': os.environ.get('PODCAST_DB_ENGINE'),
        'NAME': os.environ.get('PODCAST_DB_NAME'),
        'USER': os.environ.get('PODCAST_DB_USER'),
        #'USER': 'root',
        'PASSWORD': os.environ.get('PODCAST_DB_PASSWORD'),
        'HOST': os.environ.get('PODCAST_DB_HOST'),
        #'PORT': '5432',
    }
}

"""
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211',
    }
}
CACHALOT_ENABLED = True


DEBUG_TOOLBAR_PANELS = {
    'cachalot.panels.CachalotPanel': True
}
"""

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

LANGUAGE_CODE = 'en'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

LOCALE_PATHS = (
    os.path.join(BASE_DIR, "locale"),
)

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_URL = '/static/'
STATIC_ROOT = 'static_root'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

SITE_ID = 1

# KEYS / SECRETS

# Google Drive backup folder
GD_UPLOAD_FOLDER_PODCAST_IMG = os.environ.get('PODCAST_GD_FOLDER_IMG'),
GD_UPLOAD_FOLDER_PODCAST_LABEL_IMG = os.environ.get('PODCAST_GD_FOLDER_LABEL'),

# DISQUS
DISQUS_API_KEY = 'oSrPirH0gmyJYylFzj9boNlU7YIcq24EpmmQr72xgdIY7yWmnyNgVYGy8rchjudS'
DISQUS_WEBSITE_SHORTNAME = 'localhost'

# OAUTH
SOCIAL_AUTH_TWITTER_KEY = 'UQnmla3W4IqfHaIAQkFXYjmJl'
SOCIAL_AUTH_TWITTER_SECRET = 'FeMHN5ZQGwkXZLgslY1SqcYFA8KLSn1YMUYIUmnPLsCQ52nCoG'

SOCIAL_AUTH_FACEBOOK_KEY = '1101324529896630'
SOCIAL_AUTH_FACEBOOK_SECRET = '53b874bdea82099511dee0aa4803dce3'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '117384865423-c12rhmmd57h0mm00ql315bkd4f3f88lm.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'LABAAQ7tDtaD0q5skyqc-Ikh'
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = ['profile', 'email']

SOCIAL_AUTH_SOUNDCLOUD_KEY = 'e9cdcbabeb5b8b8f6ac84878d77dc5c8'
SOCIAL_AUTH_SOUNDCLOUD_SECRET = 'ad7b76da229bd19a24fe657e8af35bd0'

SOCIAL_AUTH_MIXCLOUD_KEY = 'SkEe3CaQ3g8R5e7SbW'
SOCIAL_AUTH_MIXCLOUD_SECRET = 'DaNXBbsSmyTLcAbkfypYBNfetjFw8jj3'

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')